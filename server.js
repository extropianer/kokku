"use strict";
/**
 * Module dependencies
 */

var fs        = require('fs');
var express   = require('express');
var mongoose  = require('mongoose');
var passport  = require('passport');
var config    = require('./app/config/config');
var log       = require('./app/config/logger');

var app = express();
var port = process.env.PORT || 3001;

// Connect to mongodb
var connect = function () {
  var options = { server: { socketOptions: { keepAlive: 1 } } };
  mongoose.connect(config.db, options);
};
connect();

mongoose.connection.on('error', console.log);
mongoose.connection.on('disconnected', connect);

// Bootstrap models
fs.readdirSync(__dirname + '/app/models').forEach(function (file) {
  if (file.indexOf('.js') > -1) {
    require(__dirname + '/app/models/' + file);
  }
});

// Bootstrap passport config
require('./app/config/passport')(passport, config);

// Bootstrap application settings
require('./app/config/express')(app, passport);

// Bootstrap routes
require('./app/config/routes')(app, passport);


app.listen(port);
log.info('Express app started on port ' + port);


/*
//create dummy group
var Group      = mongoose.model('Group');
Group.find({}).remove()
  .then(function cleared() {
    return Group({
      name: "dummy"
    }).save();
  })
  .then(function saved() {
    return Group.find({});
  }, function error(err) {
    log.error({err: err}, 'error during save');
  }).then(function success(docs) {
    log.info({groups: docs}, 'Available groups');
  });
*/


