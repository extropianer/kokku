"use strict";
/**
 * Module dependencies.
 */
var path = require('path');

var routePath = path.normalize(__dirname + '/..' + '/routes/');

/**
 * Expose
 */

module.exports = function (app, passport) {
  /**
   * Dummy routes
   */
  app.get('/', function (req, res) {
    res.send('Welcome!');
  });
  /**
   * Route definitions for API
   */
  app.use('/auth', require(routePath + 'auth').router(passport, app));

  app.use(require(routePath + 'auth').checkAuth);

  app.use('/users', require(routePath + 'users'));

  app.use('/events', require(routePath + 'events'));

  app.use('/admin/permissions', require(routePath + 'permissions'));

  /**
   * Error handling
   */

  app.use(function (err, req, res, next) {
    // treat as 404
    if (err.message && (err.message.indexOf('not found') > -1 || (err.message.indexOf('Cast to ObjectId failed') > -1))) {
      return next();
    }
    console.error(err.stack);
    // error page
    res.status(500).send('<h1>500</h1> ' + err.stack );
  });

  // other errors go in here

  // assume 404 since no middleware responded
  app.use(function (req, res, next) {
    res.status(404).send('<h1>404 - Page not found</h1>');
  });
};
