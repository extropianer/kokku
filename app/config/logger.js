"use strict";
/**
 * Created by extropianer  on 07.08.2015.
 */
var bunyan = require('bunyan');
var config = require('./config');

var logger = bunyan.createLogger({
  name: config.package.name
});


module.exports = logger;