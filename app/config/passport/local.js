"use strict";
/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy;
var User = mongoose.model('User');

/**
 * Expose
 */

module.exports = new LocalStrategy(
  function(username, password, done) {
    console.log('authenticating ' + username + ' ...');
    User.findOne({username : username}, function (err, user) {
      if (err) {
        return done(err);
      }
      console.log(user);
      if (!user) {
        return done(null, false, { message: 'Unknown user' });
      }
      if (!user.validatePassword(password, user)) {
        return done(null, false, { message: 'Invalid password' });
      }
      return done(null, user);
    });
  }
);
