"use strict";
/**
 * Module dependencies.
 */

var path    = require('path');
var extend  = require('util')._extend;
var pck     = require('../../package.json');

var development = require('./env/development');
var test = require('./env/test');
var production = require('./env/production');
var env = (process.env.NODE_ENV || 'development').toLowerCase();

var defaults = {
  root: path.normalize(__dirname + '/..'),
  package: pck
};

/**
 * Expose
 */

module.exports = {
  development: extend(development, defaults),
  test: extend(test, defaults),
  production: extend(production, defaults)
}[env];
