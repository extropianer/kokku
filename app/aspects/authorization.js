/**
 * Created by Dang on 8/17/2015.
 */
'use strict';
var interceptor = require('./interceptor');
var permissionService = require('../services/permissionService');

var ctx = {
    req: null
}

/**
 * This aspect, implemented as interceptor function, intercepts function calls to checkAuth and authorizes the user against
 * the URL path.
 *
 * Interceptors support async I/O with promises: call return next(info.args);
 * @param next
 * @param info
 * @returns {*}
 */
function isUserAllowedToAccessURLInterceptor(next, info){
    var req = info.args[0];
    var res = info.args[1];

    if(res && req && req.user){
        return permissionService.getPermissionsOfUser(req.user.username).then(function(permissions){

            if(!permissions)
                throw "[Access denied]: Could not authorize user: " + req.user.username + " reason: no Permissions defined for User";


            for(var idx = 0; idx < permissions.length; idx++){
                var permission = permissions[idx];
                if (!permission.resources)
                    continue;

                for (var i = 0; i < permission.resources.length; i++) {
                    var resource = permission.resources[i];

                    if (resource.path === req.originalUrl || resource.path + "/" === req.originalUrl) {
                        console.log("[Access granted]: User " + req.user.username + " is authorized for resource: " + req.originalUrl);

                        return next(info.args);
                    }
                }
            }
            throw "[Access denied]: Could not authorize user: " + req.user.username + " reason: User is not permitted to access resource: " + req.originalUrl;
        }).catch(function error(err){
            console.log(err);
            res.status(500).send(err);
        });
    }else{
        //not logged in yet. skip authorization.
        return next(info.args);
    }
}

/**
 * This aspect, implemented as interceptor function, intercepts function calls and authorizes the user against
 * function calls.
 * This aspect is designed to intercept I/O Operations that return promises (our service-methods).
 * For other types of function call use a different interceptor.
 *
 * Interceptors support async I/O with promises: call return next(info.args);
 * @param next
 * @param info
 * @returns {*}
 */
function isUserAllowedToUseServiceFunctionInterceptor(next, info){
    return permissionService.getPermissionsOfUser(ctx.req.user.username).then(function(permissions){

        if(!permissions)
            throw "[Access denied]: Could not authorize user: " + ctx.req.user.username + " reason: no Permissions defined for User";


        for(var idx = 0; idx < permissions.length; idx++){
            var permission = permissions[idx];
            if (!permission.resources)
                continue;

            for (var i = 0; i < permission.resources.length; i++) {
                var resource = permission.resources[i];

                if (resource.method === info.method || resource.method === "*") {
                    console.log("[Access granted]: User " + ctx.req.user.username + " is authorized for service-method: " + info.method);

                    return next(info.args);
                }
            }
        }
        throw "[Access denied]: Could not authorize user: " + ctx.req.user.username + " reason: User is not permitted to call service-method: " + info.method;
    }).catch(function error(err){
        console.log(err);
        //rethrow as though as it was from the original function
        throw err;
        //ctx.res.status(500).send(err);
    });
}

//Method and URL level authorization
module.exports = {
    applyURLInterceptor: function(instance, pointcut){
        interceptor.intercept(isUserAllowedToAccessURLInterceptor, instance, pointcut);
    },
    applyServiceInterceptor: function(instance, pointcut){
        interceptor.intercept(isUserAllowedToUseServiceFunctionInterceptor, instance, pointcut);
    },

    context: function (context){
        ctx = context;
    }
}