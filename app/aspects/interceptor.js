/**
 * Created by Dang on 8/20/2015.
 */
'use strict';

function matchPointcut(tobeMatched, pointcut){
    if(pointcut instanceof RegExp){
        return pointcut.test(tobeMatched.toString());
    }else if(tobeMatched === pointcut || '*' === pointcut){
        return true;
    }
    return false;
}

//helper function to handle looping in closures.
function createNext(original, instance){
    return function(args){
        console.log("Calling Original-Function "); // + Array.prototype.join.call(args));
        return original.apply(instance, args);
    }
}
//helper function to handle looping in closures.
function createInterceptor(next, interceptor, instance, prop){
    return function(){
        console.log("Calling Interceptor() for " + prop); // + "(" + Array.prototype.join.call(arguments) + ")");
        return interceptor(next, {target: instance, method: prop, args: arguments});
    }
}

/**
 * API: : intercepts a function call by it's name via pattern or concrete string. Subsequent Calls will be
 * intercepted by the to be implemented interceptor function(next, info). When intercepter is finished call
 * next(info.args) to call the original function.
 *
 * Supports asynchronous I/O via next function (call next inside the promise or callback).
 *
 * @param interceptor the interceptor function(next, info) to be defined by the aspect.
 * @param instance the instance on which the aspect works.
 * @param pointcut the pattern for matching functions.
 */
function intercept(interceptor, instance, pointcut){
    console.log("Applying Interceptor for '" + pointcut + "'");
    for(var prop in instance) {
        if(typeof instance[prop] === "function" && matchPointcut(prop, pointcut)) {

            var next = createNext(instance[prop], instance);
            instance[prop] = createInterceptor(next, interceptor, instance, prop);
            console.log("Interceptor registered for '" + prop + "'");
        }
    }
}

module.exports = {
    intercept: intercept
}