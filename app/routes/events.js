'use strict';
/**
 * Created by extropianer on 07.08.2015.
 */


var express     = require('express');
var router      = express.Router();
var userService = require('../services/eventService');
var log         = require('../config/logger');

// middleware specific to this router
router.use(function timeLog(req, res, next) {
  console.log('Time: ', Date.now());
  next();
});
// TODO: Check authentication
router.post('/', function eventPost(req, res) {

  log.info({user: req.body}, 'Creating new event');
  res.send(req.body);
});

router.put('/:event_id', function eventPut(req, res) {

});
router.delete('/:event_id', function eventDelete(req, res) {

});
router.get('/:group_id', function eventGetGroup(req, res) {

});

module.exports = router;