/**
 * Created by Dang on 8/16/2015.
 */
'use strict';

var express = require('express');
var router = express.Router();
var permissionService = require('../services/permissionService');

router.get('/', function(req, res) { // TODO: restrict to admins
    permissionService.listPermissions().then(function success(docs) {
        res.send(docs);
    }); // Admin only
});

router.get('/:permission', function(req, res) {
    // get permission data
    permissionService.readPermission(req.params.permission).then(function success(permission)
    {
        console.log("Permission: " + req.params.permission + " fetched successfully");
        res.send(permission);
    }, function error(err) {
        res.status(500).send(err);
    });
});


router.post('/:permission', function(req, res) {
    //Todo: add authorization
    var permission = {
        name:  req.params.permission,
        resources: req.body.resources,
        users: req.body.users,
    };

    console.log(permission);
    permissionService.createPermission(permission).then(function success(permission) {
        console.log("Permission: " + req.params.permission + " created successfully");
        res.send(permission);
    }, function error(err) {
        res.status(500).send(err);
    });
});

router.put('/:permission', function(req, res) {
    var permission = {
        name:  req.params.permission,
        resources: req.body.resources,
        users: req.body.users,
    };
    console.log(permission);
    permissionService.updatePermission(permission).then(function success(permission)
    {
        console.log("Permission: " + req.params.permission + " updated successfully");
        res.send(permission);
    }, function error(err) {
        res.status(500).send(err);
    });
});

router.put('/:permission/:user', function(req, res) {
    console.log(req.params.permission + "/" + req.params.user + " (P/U) called");
});


router.put('/:permission/:resource', function(req, res) {
    console.log(req.params.permission + "/" + req.params.resource + " (P/R) called");
});

router.delete('/:permission', function(req, res) {
    permissionService.deletePermission(req.params.permission)
        .then(function success(permission) {
            console.log("Permission: " + req.params.permission + " deleted successfully");
            res.send(permission);
        }, function error(err) {
            res.status(500).send(err);
        });
});

module.exports = router;