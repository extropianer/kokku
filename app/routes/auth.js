/**
 * Created by Dang on 8/12/2015.
 */
'use strict';

var express = require('express');
var router = express.Router();
var userService = require('../services/userService');
var local = require('../config/passport/local');

function initAuth(passport, app) {

    router.get('/', function (req, res) {
        //login page
        //TODO: do it correctly

        var html = '<html><body>Login Page<br>';
        html += '<form action="/auth/login" method="post">';
        html += '<div><label>Username: </label><input type="text" name="username"/></div>';
        html += '<div><label>Password: </label><input type="password" name="password"/></div>';
        html += '<div><input type="submit" value="Log In"/></div>';
        html += '</form></body></html>';
        res.send(html);
    });

    router.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/auth');
    });

    router.post('/login',
        passport.authenticate('local'), function(req, res) {
            //auth successful
            res.redirect('/users/' + req.user.username);
    });

    return router;

}

function isLoggedIn(req, res, next) {
    if (!req.user)
        return res.redirect('/auth');
    next();
};

module.exports = {
    router : initAuth,
    checkAuth : isLoggedIn
}

var authorization = require('../aspects/authorization');
authorization.applyURLInterceptor(module.exports, /checkAuth/);
