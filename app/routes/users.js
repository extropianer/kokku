'use strict';
/**
 * Created by extropianer on 05.08.2015.
 */
var express = require('express');
var router = express.Router();
var userService = require('../services/userService');

router.get('/', function(req, res) { // TODO: restrict to admins
  userService.useWith(req, res).listUsers().then(function success(docs) {
    res.send(docs);
  },  function error(err) {
    res.status(500).send(err);
  });
});

router.get('/:user', function(req, res) {
  // get user data
  userService.useWith(req, res).readUser(req.params.user).then(function success(user)
  {
    console.log("User: " + req.params.user + " fetched successfully");
    res.send(user);
  }, function error(err) {
    res.status(500).send(err);
  });
});


router.post('/:user', function(req, res) {

  var user = {
    name:  req.body.name,
    firstname: req.body.firstname,
    username: req.params.user,
    email: req.body.email,
    password: req.body.password
  };

  console.log(user);
  userService.useWith(req, res).createUser(user).then(function success(user) {
    console.log("User: " + req.params.user + " created successfully");
    res.send(user);
  }, function error(err) {
    res.status(500).send(err);
  });
});

router.put('/:user', function(req, res) {
  // update user data
  var user = {
    name:  req.query.name,
    firstname: req.query.firstname,
    username: req.params.user,
    email: req.query.email
  };
  console.log(user);
  userService.useWith(req, res).updateUser(user).then(function success(user)
  {
    console.log("User: " + req.params.user + " updated successfully");
    res.send(user);
  }, function error(err) {
    res.status(500).send(err);
  });
});

router.delete('/:user', function(req, res) { // TODO: Make sure only admins can delete
  // delete user
  // 1. user logged in, deleting self
  // 2. admin deleting user
  userService.useWith(req, res).deleteUser(req.params.user)
    .then(function success(user) {
        console.log("User: " + req.params.user + " deleted successfully");
        res.send(user);
    }, function error(err) {
      res.status(500).send(err);
    });
});

module.exports = router;