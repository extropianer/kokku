"use strict";
/**
 * Created by offis_000 on 05.08.2015.
 */

/*!
 * Module dependencies
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ObjectId = Schema.ObjectId;
/**
 * schema
 */

var GroupSchema = new Schema({
  name: { type: String, required: true, unique: true },
  created: { type: Date, default: Date.now },
  members: [{
    user: { type: ObjectId, required: true}
  }],
  start_location: { type: String },
  end_location: { type: String },
  distance: { type: Number },
  recurrence: [{
    type: String
  }] // daily, weekdays, weekly, monthly
});

/**
 * plugin
 */



/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

GroupSchema.method({

});

/**
 * Statics
 */

GroupSchema.static({

});

/**
 * Register
 */

mongoose.model('Group', GroupSchema);
