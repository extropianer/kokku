/**
 * Created by Dang on 8/13/2015.
 */
"use strict";
/*!
 * Module dependencies
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ObjectId = Schema.ObjectId;
/**
 * schema
 */

var PermissionSchema = new Schema({
    name:  {type: String, unique: true},
    resources: [{
        path: {type: String},
        method: {type: String}
    }],
    users: [{
        user: {type: String, required: true}
    }],
    created: { type: Date, default: Date.now }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Pre-save hook
 */


/**
 * Methods
 */

PermissionSchema.methods = {
};

/**
 * Statics
 */

PermissionSchema.static({ });

/**
 * Register
 */

mongoose.model('Permission', PermissionSchema);
