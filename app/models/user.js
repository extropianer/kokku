"use strict";
/*!
 * Module dependencies
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ObjectId = Schema.ObjectId;
/**
 * schema
 */

var UserSchema = new Schema({
  name:  String,
  firstname: String,
  birthdate: {type: Date, default: new Date(2000, 1, 1).toJSON()},
  username: { type: String, unique: true},
  email: String,
  created: { type: Date, default: Date.now },
  hashed_password: String,
  salt: String,
  admin: Boolean,

  statistics: [{
    group_id: ObjectId,
    drives_made: Number,
    ppl_transported: Number,
    drives_taken: Number,
    distance_traveled: Number
  }]

});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Pre-save hook
 */


/**
 * Methods
 */

UserSchema.methods = {
  validatePassword: function (password, user){
    if(this.hashCode(password) == user.hashed_password)
      return true;

    return false;
  },
  hashCode : function(passw) {
    var hash = 0, i, chr, len;
    if (passw.length == 0) return hash;
    for (i = 0, len = passw.length; i < len; i++) {
      chr   = passw.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }
};

/**
 * Statics
 */

UserSchema.static({ });

/**
 * Register
 */

mongoose.model('User', UserSchema);
