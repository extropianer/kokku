"use strict";
/**
 * Created by offis_000 on 05.08.2015.
 */

/*!
 * Module dependencies
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ObjectId = Schema.ObjectId;
/**
 * schema
 */
var EventSchema = new Schema({
  group: {type: ObjectId, required: true},
  driver: {type: ObjectId, required: true},
  date: {type: Date, default: Date.now()},
  passengers: [{
    user: {type: ObjectId, required: true},
    go_trip: { type: Boolean, default: true},
    return_trip: { type: Boolean, default: true}
  }],
  comments: [{
    body: { type: String, required: true },
    date: { type: Date, default: Date.now() },
    author: { type: ObjectId, required: true }
  }],
  go_start: {type: Date, required: true},
  return_start: {type: Date, required: true}
});



/**
 * plugin
 */



/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */

EventSchema.method({

});

/**
 * Statics
 */

EventSchema.static({

});

/**
 * Register
 */

mongoose.model('Event', EventSchema);
