"use strict";
/**
 * Created by extropianer on 07.08.2015.
 */
/**
 * Module dependencies.
 */

var mongoose  = require('mongoose');
var q         = require('q');
var Event      = mongoose.model('Event');


// dummyGroup = '55c5202eec8087781406d3b4';
function createEvent(event) {
  return new Event(event).save();
}

function getEvents() {
  return Event.find({});
}

function deleteEvent(event_id) {
  return Event.remove({ _id: event_id });
}


module.exports = {
  createEvent: createEvent,
  getEvents: getEvents,
  deleteEvent: deleteEvent
};