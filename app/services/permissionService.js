"use strict";
/**
 * Created by Dang on 8/16/2015.
 */

/**
 * Module dependencies.
 */

var mongoose  = require('mongoose');
var q         = require('q');
var Permission = mongoose.model('Permission');

function listPermissions() {
    return Permission.find({});
}

/**
 * Create user
 */

function createPermission(permission) {
    return new Permission(permission).save();
}

function readPermission(name){
    return Permission.find({name: name});
}

function updatePermission(permission){
    return Permission.update({name: permission.name}, {$set: {resources: permission.resources, users: permission.users}});
}

function deletePermission(name) {
    return Permission.remove({ name: name });
}

function getPermissionsOfUser(username, callback){
    return Permission.find({"users.user" : username}, callback);
}

function addUserToPermission(user, p_name){
    return Permission.update({name: p_name}, {$push: {users: user.username}});
}

function addResourceToPermission(resource, p_name){
    return Permission.update({name: p_name}, {$push: {resources: resource}});
}

function removeUserFromPermission(user, p_name){
    return Permission.update({name: p_name}, {$pull: {users: user.name}});
}

function removeResourceFromPermission(resource, p_name){
    return Permission.update({name: p_name}, {$pull: {resources: resource}});
}

module.exports = {
    listPermissions: listPermissions,
    createPermission: createPermission,
    readPermission: readPermission,
    updatePermission: updatePermission,
    deletePermission: deletePermission,
    getPermissionsOfUser: getPermissionsOfUser,
    addUserToPermission: addUserToPermission,
    addResourceToPermission: addResourceToPermission,
    removeUserFromPermission: removeUserFromPermission,
    removeResourceFromPermission: removeResourceFromPermission
};