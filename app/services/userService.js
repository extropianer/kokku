"use strict";
/**
 * Created by extropianer on 05.08.2015.
 */

/**
 * Module dependencies.
 */
var mongoose  = require('mongoose');
var q         = require('q');
var User      = mongoose.model('User');

/* List all Users */
function listUsers() {
  return User.find({});
}

/* Create User */
function createUser(user) {
  user.hashed_password = new User().hashCode(user.password);
  return new User(user).save();
}

/* Read User */
function readUser(username){
  return User.find({username: username});
}

/* Update User */
function updateUser(user){
  return User.update({username: user.username}, {$set: {name: user.name, firstname: user.firstname, email: user.email}});
}

/* Delete User */
function deleteUser(username) {
  return User.remove({ username: username });
}

/**
 * Unfortunately this aspect requires context information (req), therefore cross-cutting concern is not really separated
 * from this Module
 * Todo: cut this dependency, authorization is a cross cutting concern,
 * it should a possible to access request information in a global way
 **/
function useWith(request, response){
  authorization.context({
    req: request,
    res: response
  });
  return module.exports;
}

module.exports = {
  listUsers: listUsers,
  createUser: createUser,
  readUser: readUser,
  updateUser: updateUser,
  deleteUser: deleteUser,
  useWith: useWith
};


var authorization = require('../aspects/authorization');
//applyAspect on everything that is not 'useWith'
authorization.applyServiceInterceptor(module.exports, /(listUsers|createUser|readUser|updateUser|deleteUser)/);
