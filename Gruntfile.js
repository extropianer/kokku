"use strict";

module.exports = function(grunt) {

  // Load Grunt tasks declared in the package.json file
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);


  grunt.initConfig({
    // vagrant provisioning
    vagrant_commands: {
      boot: {
        commands: [
          // each command in this array as an array arguments.
          ['up']
        ]
      },
      shutdown: {
        commands: [
          ['halt', '-f']
        ]
      }
    },
    // style checks
    jshint: {
      // default options for all sub tasks
      options: {
        "bitwise"       : true,     // Prohibit bitwise operators (&, |, ^, etc.).
        "curly"         : true,     // Require {} for every new block or scope.
        "eqeqeq"        : true,     // Require triple equals i.e. `===`.
        "forin"         : true,     // Tolerate `for in` loops without `hasOwnPrototype`.
        "immed"         : true,     // Require immediate invocations to be wrapped in parens e.g. `( function(){}() );`
        "latedef"       : true,     // Prohibit variable use before definition.
        "newcap"        : true,     // Require capitalization of all constructor functions e.g. `new F()`.
        "noarg"         : true,     // Prohibit use of `arguments.caller` and `arguments.callee`.
        "noempty"       : true,     // Prohibit use of empty blocks.
        "nonew"         : true,     // Prohibit use of constructors for side-effects.
        "plusplus"      : true,     // Prohibit use of `++` & `--`.
        "regexp"        : true,     // Prohibit `.` and `[^...]` in regular expressions.
        "undef"         : true,     // Require all non-global variables be declared before they are used.
        "unused"        : "paramsigonre",     // Prohibit unused variables, ignore unused function arguments
        "strict"        : true,     // Require `use strict` pragma in every file.
        "trailing"      : true,     // Prohibit trailing whitespaces.
        "globalstrict"  : true      // Enforce global strict mode on all files
      },
      server: {
        files: {
          src: ['Gruntfile.js', 'server.js', 'app/**/*.js']
        },
        options: {
          node: true,   // assume node keywords as global variables
          force: true   // keep grunt running after finding errors
        }
      }
    },
    // set environment variables
    env : {
      options: {
        //Shared Options Hash
      NODE_PATH: './app'
      },
      dev: {
        NODE_ENV: 'development'
      }
    },

    // configure nodemon
    nodemon: {
      dev: {
        script: 'server.js'
      }
    },

    // watch for file changes and jshint again
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint']
    }
  });

  // Creates the default task
  grunt.registerTask('default', ['vagrant_commands:boot', 'jshint:server', 'nodemon']);

};