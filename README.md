# Kokku

A carpooling manager.

## Installation and Usage

    $ vagrant up
    $ npm install
    $ npm install -g grunt-cli
    $ bower install
    $ npm start

Add routes (`app/routes.js`), create models (`app/models/`) and services (`app/services/`).

## License

MIT